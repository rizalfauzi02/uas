<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title; ?></title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?= base_url('assets-app/'); ?>assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?= base_url('assets-app/'); ?>assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="<?= base_url('assets-app/'); ?>assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="<?= base_url('assets-app/'); ?>assets/images/favicon.ico" />

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?= base_url('app-assets/'); ?>assets/vendors/sweetalert2/dist/notiflix-2.6.0.min.css">
    <script src="<?= base_url('app-assets/'); ?>assets/vendors/sweetalert2/dist/notiflix-2.6.0.min.js"></script>
    <script src="<?= base_url('app-assets/'); ?>assets/vendors/sweetalert2/dist/sweetalert2.all.js"></script>
    <!-- SWEETALERT FIX -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-5 mx-auto">
              <div class="auth-form-light text-left p-5">
                <?= $this->session->flashdata('pesan'); ?>
                <div class="brand-logo">
                  <img src="<?= base_url('assets-app/'); ?>assets/images/logo.svg">
                </div>
                <h4>REGISTRASI ADMIN</h4>
                <h6 class="font-weight-light">Silahkan melakukan daftar akun untuk admin</h6>

                <form class="pt-3" method="POST" action="<?= base_url('install/prosesReg') ?>">
                  <div class="form-group">
                    <input type="text" name="namalengkap" class="form-control form-control-lg" id="exampleInputUsername1" placeholder="Nama Lengkap" required>
                  </div>
                  <div class="form-group">
                    <input type="text" name="username" class="form-control form-control-lg" id="exampleInputUsername1" placeholder="Username" required>
                  </div>
                  <!-- <div class="form-group">
                      <input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" required>
                    </div> -->
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="password" class="form-control form-control-user" id="password1" name="password1" placeholder="Password">
                        </div>
                        <div class="col-sm-6">
                            <input type="password" class="form-control form-control-user" id="password2" name="password2" placeholder="Ulangi Password">
                        </div>
                    </div>
                    <div class="form-group">
                      <input type="email" name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email" required>
                    </div>
                  <div class="mt-3">
                    <!-- <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="<?= base_url('assets-app/'); ?>">SIGN UP</a> -->
                    <button type="submit" class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn">Register</button>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?= base_url('assets-app/'); ?>assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<?= base_url('assets-app/'); ?>assets/js/off-canvas.js"></script>
    <script src="<?= base_url('assets-app/'); ?>assets/js/hoverable-collapse.js"></script>
    <script src="<?= base_url('assets-app/'); ?>assets/js/misc.js"></script>
    <!-- endinject -->
  </body>
</html>