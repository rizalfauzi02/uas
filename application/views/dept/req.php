        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Form Request </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url('Dept/request') ?>">List</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Form Request</li>
                </ol>
              </nav>
            </div>
            <div class="row">
              <div class="col-md-9 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Request Barang</h4><br>

                <form action="<?= base_url('dept/prosesSubmit'); ?>" method="POST">
                    <div class="form-group">
                        <label for="">Nama Barang</label>
                        <!-- <input type="text" class="form-control" name="namabarang" required> -->
                        <select name="namabarang" id="namabarang" class="form-control" required>
                            <?php foreach($barang as $data) { ?>
                                <option value="<?= $data->merk; ?>"><?= $data->merk; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Qty</label>
                        <input type="number" min="0" id="qty" class="form-control" name="qty" onkeyup="hitung();" required/>
                    </div>
                    <div class="form-group">
                        <label for="">Satuan</label>
                        <input type="text" class="form-control" name="satuan">
                    </div>
                    <div class="form-group">
                        <label for="">Harga</label>
                        <input type="number" min="0" class="form-control" id="harga" name="harga" onkeyup="hitung();" required/>
                    </div>
                    <div class="form-group">
                        <label for="">Total Harga</label>
                        <input type="number" min="0" name="totharga" class="form-control" id="total" readonly required/>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" value="On Proses" name="status">
                    </div>
                    <button type="submit" class="btn btn-primary">Send</button> 
                    <button type="reset" class="btn btn-secondary">Reset</button>
                </form>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- partial -->
        </div>
<script>
  function hitung() {
      // Mengambil Nilai dari yang diketik oleh user
        var qty = document.getElementById('qty').value;
        var harga = document.getElementById('harga').value;

      // Dimasukkan result untuk dilakukan perkalian
        var result = parseInt(qty) * parseInt(harga);
    
        if (!isNaN(result)) {
          document.getElementById('total').value = result;
        }
  }
</script>