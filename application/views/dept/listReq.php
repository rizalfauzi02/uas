        <!-- CONTENT ==================== -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-format-list-bulleted"></i>
                </span> List Request
              </h3>
            </div>
<?= $this->session->flashdata('pesan'); ?>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">                     
                        <a href="<?= base_url('dept/req') ?>" class="btn btn-gradient-primary btn-fw float-right">Request</a><br>
                        <table class="table table-striped">
                      <thead>
                        <tr align="center">
                            <th>#</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                            <th>Harga Satuan</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                            <!-- <th>Action</th> -->
                        </tr>
                      </thead>
                      <tbody align="center">
                            </tr>
                            <?php $no=1;
                            foreach ($list as $row)
                            {
                            ?>
                        <tr>
                            <td width="80px"><?php echo $no++ ?></td>
                            <td><?php echo $row->namabarang ?></td>
                            <td><?php echo $row->qty ?></td>
                            <td><?php echo $row->satuan ?></td>
                            <td><?php echo $row->harga ?></td>
                            <td><?php echo $row->totharga ?></td>
                            <td><label class="badge badge-gradient-danger"><?= $row->status; ?></label></td>
                            <!-- <td style="text-align:center" width="200px">
                                <?php 
                                echo anchor(site_url('row/read/'.$row->id),'Read', 'class="btn btn-gradient-primary btn-sm"'); 
                                echo ' | '; 
                                echo anchor(site_url('row/update/'.$row->id),'Update', 'class="btn btn-gradient-success btn-sm"'); 
                                echo ' | '; 
                                // echo anchor(site_url('row/delete/'.$row->id),'Delete','onclick="javasciprt: return confirm(\'Data akan dihapus?\')"'); 
                                ?>
                                <a href="<?= base_url('row/delete/') . $row->id; ?>" style="text-decoration:none" 
                                  class="btn btn-gradient-danger btn-sm hapus" onclick="return confirm('Data akan dihapus?')">  <!-- SWEETALERT BUTTON BELUM JALAN -->
                                    <!-- <i class="fas fa-trash"></i> Delete
                                </a>
                            </td> -->
                        </tr>
                                <?php
                            }
                            ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
         