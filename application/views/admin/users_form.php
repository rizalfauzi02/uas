        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Input User </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url('admin') ?>">User</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Tambah User</li>
                </ol>
              </nav>
            </div>
            <div class="row">
              <div class="col-md-9 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Tambah User</h4><br>
                    
                    <form class="forms-sample" action="<?php echo $action; ?>" method="post">
                      	<div class="form-group">
                            <label for="varchar">Namalengkap <?php echo form_error('namalengkap') ?></label>
                            <input type="text" class="form-control" name="namalengkap" id="namalengkap" placeholder="Namalengkap" value="<?php echo $namalengkap; ?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="varchar">Email </label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" required/>
                            <?= form_error('email', '<small class="text-danger pl-1">', '</small>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="varchar">Username</label>
                            <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" required/>
                            <?php echo form_error('username') ?>
                        </div>
                        <?php if($button == "Create"){?>
                      <div class="form-group">
                            <label for="varchar">Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
                            <?php echo form_error('password') ?>
                        </div>
                        <?php }else{?>
                            <input type="hidden" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
                        <?php } ?>
                        <!-- <div class="form-group">
                            <label for="varchar">Password </label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" required/>
                            <?= form_error('password', '<small class="text-danger pl-1">', '</small>'); ?>
                        </div> -->
                        <div class="form-group">
                            <label for="varchar">Akses <?php echo form_error('akses') ?></label>
                            <select name="akses" id="akses" class="form-control">

                              <?php if($button == "Create"){ ?>
                                <option value="1">Admin</option>
                                <option value="2">Operator Gudang</option>
                                <option value="3">Kepala Gudang</option>
                                <option value="4">Penjaga Gudang</option>
                                <option value="5">Dept Penjualan</option>
                                <?php }else{ ?>
                                 <?php if($akses == 1) : ?>
                                    <option value="<?= $akses; ?>">Admin</option>
                                    <option value="2">Operator Gudang</option>
                                    <option value="3">Kepala Gudang</option>
                                    <option value="4">Penjaga Gudang</option>
                                    <option value="5">Dept Penjualan</option>
                                <?php elseif($akses == 2) : ?>
                                    <option value="<?= $akses; ?>">Operator Gudang</option>
                                    <option value="1">Admin</option>
                                    <option value="3">Kepala Gudang</option>
                                    <option value="4">Penjaga Gudang</option>
                                    <option value="5">Dept Penjualan</option>
                                <?php elseif($akses == 3) : ?>
                                    <option value="<?= $akses; ?>">Kepala Gudang</option>
                                    <option value="1">Admin</option>
                                    <option value="2">Operator Gudang</option>
                                    <option value="4">Penjaga Gudang</option>
                                    <option value="5">Dept Penjualan</option>
                                <?php elseif($akses == 4) : ?>
                                    <option value="<?= $akses; ?>">Penjaga Gudang</option>
                                    <option value="1">Admin</option>
                                    <option value="2">Operator Gudang</option>
                                    <option value="3">Kepala Gudang</option>
                                    <option value="5">Dept Penjualan</option>
                                <?php elseif($akses == 5) : ?>
                                    <option value="<?= $akses; ?>">Dept Penjualan</option>
                                    <option value="1">Admin</option>
                                    <option value="2">Operator Gudang</option>
                                    <option value="3">Kepala Gudang</option>
                                    <option value="4">Penjaga Gudang</option>
                                <?php endif; ?>
                            <?php } ?>
                            
                            </select>
                        </div>
                      <input type="hidden" name="isActive" value="1">
                      <input type="hidden" name="id_users" value="<?php echo $id_users; ?>" />
                      <button type="submit" class="btn btn-gradient-primary mr-2"><?php echo $button ?></button>
                      <a href="<?php echo site_url('admin') ?>" class="btn btn-light">Cancel</a>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- partial -->
        </div>