        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Update User </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url('admin') ?>">User</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Tambah User</li>
                </ol>
              </nav>
            </div>
            <div class="row">
              <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Update Data</h4><br>
                    
                    <form class="forms-sample" action="<?php echo $action; ?>" method="post">
                        <div class="form-group">
                            <label for="varchar">Username</label>
                            <input type="text" class="form-control" id="username" placeholder="Username" value="<?php echo $username; ?>" readonly/>
                            <?php echo form_error('username') ?>
                        </div>
                      	<div class="form-group">
                            <label for="varchar">Namalengkap <?php echo form_error('namalengkap') ?></label>
                            <input type="text" class="form-control" name="namalengkap" id="namalengkap" placeholder="Namalengkap" value="<?php echo $namalengkap; ?>" />
                        </div>
                        <div class="form-group">
                            <label for="varchar">Email </label>
                            <input type="email" class="form-control" name="email2" id="email" placeholder="Email" value="<?php echo $email; ?>"/>
                            <?= form_error('email2', '<small class="text-danger pl-1">', '</small>'); ?>
                        </div>
                        <div class="form-group">
                            <label for="varchar">Akses <?php echo form_error('akses') ?></label>
                            <select name="akses" id="akses" class="form-control">
                                <?php if($akses == 1) : ?>
                                    <option value="<?= $akses; ?>">Admin</option>
                                    <option value="2">Operator Gudang</option>
                                    <option value="3">Kepala Gudang</option>
                                    <option value="4">Penjaga Gudang</option>
                                    <option value="5">Dept Penjualan</option>
                                <?php elseif($akses == 2) : ?>
                                    <option value="<?= $akses; ?>">Operator Gudang</option>
                                    <option value="1">Admin</option>
                                    <option value="3">Kepala Gudang</option>
                                    <option value="4">Penjaga Gudang</option>
                                    <option value="5">Dept Penjualan</option>
                                <?php endif; ?>
                            </select>
                        </div>
                      <input type="hidden" name="isActive" value="1">
                      <input type="hidden" name="id_users" value="<?php echo $id_users; ?>" />
                      <button type="submit" class="btn btn-gradient-primary mr-2"><?php echo $button1 ?></button>
                      <a href="<?php echo site_url('admin') ?>" class="btn btn-light">Cancel</a>
                    </form>
                  </div>
                </div>
              </div>

              <div class="col-md-6 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Ubah Password</h4><br>
                    
                        <form action="<?= $ubah; ?>" method="POST">
                            <div class="form-group">
                                <label for="varchar">Password Lama</label>
                                <input type="password" class="form-control" name="currentpassword" placeholder="Password Lama" required>
                            </div>
                            <div class="form-group">
                                <label for="varchar">Password Baru</label>
                                <input type="password" class="form-control" name="newpassword" placeholder="Password Baru" required>
                            </div>
                            <button type="submit" class="btn btn-primary float-right"><?= $button2; ?></button>
                            <input type="hidden" name="id_users" value="<?php echo $id_users; ?>" />
                            <input type="hidden" name="isActive" value="1">
                        </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- partial -->
        </div>