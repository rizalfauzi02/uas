        <!-- CONTENT ==================== -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-account"></i>
                </span> Read Users
              </h3>
            </div>
<?= $this->session->flashdata('pesan'); ?>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Users
                    <!-- <button type="button" class="btn btn-gradient-primary btn-fw float-right">Tambah User</button> -->
                    <a href="<?= base_url('admin') ?>" class="btn btn-gradient-secondary btn-fw float-right">Kembali</a>
                    </h4><br>
                    <table class="table table-striped">
                      <thead>
                        <tr align="center">
                            <th>Namalengkap</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Akses</th>
                        </tr>
                      </thead>
                      <tbody>
                            <tr align="center">
                                <td><?php echo $namalengkap; ?></td>
                                <td><?php echo $username; ?></td>
                                <td><?php echo $email; ?></td>
                                <td><?php if($akses == 1){echo "Admin";}elseif($akses == 2){echo "Operator";}elseif($akses == 3){echo "Kepala";}elseif($akses == 4){echo "Penjaga";}elseif($akses == 5){echo "Dep.Penjualan";} ?></td>
                            </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              <!-- <div class="col-lg-4 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title">Keterangan Akses:</h5>
                    <table class="table table-striped">
                      <tbody>
                        <tr>
                          <td>Admin : </td>
                          <td>1</td>
                        </tr>
                        <tr>
                          <td>Operator Gudang : </td>
                          <td>2</td>
                        </tr>
                        <tr>
                          <td>Kepala Gudang : </td>
                          <td>3</td>
                        </tr>
                        <tr>
                          <td>Penjaga Gudang : </td>
                          <td>4</td>
                        </tr>
                        <tr>
                          <td>Dept. Penjualan : </td>
                          <td>5</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div> -->
          </div>
          <!-- content-wrapper ends -->
         