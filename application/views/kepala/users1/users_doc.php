<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets-app/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Users List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
            <th>Namalengkap</th>
            <th>Username</th>
            <th>Email</th>
            <th>Akses</th>
            <th>IsActive</th>
            <th>Created At</th>
            <th>Updated At</th>
		
            </tr><?php
            foreach ($users1_data as $users1)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $users1->namalengkap ?></td>
		      <td><?php echo $users1->username ?></td>
		      <td><?php echo $users1->email ?></td>
		      <td><?php echo $users1->akses ?></td>
		      <td><?php echo $users1->isActive ?></td>
		      <td><?php echo $users1->created_at ?></td>
		      <td><?php echo $users1->updated_at ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>