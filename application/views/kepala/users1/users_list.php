        <!-- CONTENT ==================== -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-account"></i>
                </span> Users List
              </h3>
            </div>
<?= $this->session->flashdata('pesan'); ?>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Users
                    <!-- <button type="button" class="btn btn-gradient-primary btn-fw float-right">Tambah User</button> -->
                    <!-- <a href="<?= base_url('row/create') ?>" class="btn btn-gradient-primary btn-fw float-right">Tambah User</a> -->
                    </h4><br>
                    <table class="table table-striped">
                      <thead>
                        <tr align="center">
                            <th>#</th>
                            <th>Namalengkap</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Akses</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        foreach ($users1_data as $row)
                        {
                            ?>
                        <tr align="center">
                            <td width="80px"><?php echo ++$start ?></td>
                            <td><?php echo $row->namalengkap ?></td>
                            <td><?php echo $row->username ?></td>
                            <td><?php echo $row->email ?></td>
                            <td><?php if($row->akses == 1){echo "Admin";}elseif($row->akses == 2){echo "Operator";}elseif($row->akses == 3){echo "Kepala";}elseif($row->akses == 4){echo "Penjaga";}elseif($row->akses == 5){echo "Dep.Penjualan";} ?></td>
                              </tr>
                            <?php
                        }
                        ?>
                      </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
                            <?php echo anchor(site_url('Kepala/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                            <?php echo anchor(site_url('Kepala/word'), 'Word', 'class="btn btn-primary"'); ?>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
