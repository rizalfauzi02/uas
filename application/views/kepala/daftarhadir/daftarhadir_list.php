        <!-- CONTENT ==================== -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-account"></i>
                </span> Daftar Hadir
              </h3>
            </div>
<?= $this->session->flashdata('pesan'); ?>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body"> <br>                      
                        <table class="table table-striped">
                      <thead>
                        <tr align="center">
                            <th>#</th>
                            <th>Nama Karyawan</th>
                            <th>Status</th>
                            <th>Tanggal Absen</th>
                        </tr>
                      </thead>
                      <tbody align="center">
                            </tr>
                                <?php
                                foreach ($daftarhadir_data as $daftarhadir)
                                {
                                    ?>
                                    <tr>
                                <td width="80px"><?php echo ++$start ?></td>
                                <td><?php echo $this->db->get_where('users',array('id_users' => $daftarhadir->id_pengguna))->row('namalengkap'); ?></td>
                                <td><?php echo $daftarhadir->status ?></td>
                                <td><?php echo $daftarhadir->created_at ?></td>
                                <td style="text-align:center" width="200px">
                                    <?php 
                                    // echo anchor(site_url('daftarhadir/read/'.$daftarhadir->id),'Read'); 
                                    // echo ' | '; 
                                    // echo anchor(site_url('daftarhadir/update/'.$daftarhadir->id),'Update'); 
                                    // echo ' | '; 
                                    // echo anchor(site_url('daftarhadir/delete/'.$daftarhadir->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                                    ?>
                                <a href="<?= base_url('daftarhadir/delete/') . $daftarhadir->id; ?>" style="text-decoration:none" 
                                  class="btn btn-gradient-danger btn-sm hapus" onclick="return confirm('Data akan dihapus?')">  <!-- SWEETALERT BUTTON BELUM JALAN -->
                                    <!-- <i class="fas fa-trash"></i> -->Delete
                                </a>
                                </td>
                            </tr>
                                    <?php
                                }
                                ?>
                      </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                          <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
                          <?php echo anchor(site_url('daftarhadir/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                          <?php echo anchor(site_url('daftarhadir/word'), 'Word', 'class="btn btn-primary"'); ?>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
         