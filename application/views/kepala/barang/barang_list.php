        <!-- CONTENT ==================== -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-account"></i>
                </span> Barang List
              </h3>
            </div>
<?= $this->session->flashdata('pesan'); ?>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">                      
                        <!-- <a href="<?= base_url('barang/create') ?>" class="btn btn-gradient-primary btn-fw float-right">Tambah Barang</a><br> -->
                        <table class="table table-striped">
                      <thead>
                        <tr align="center">
                            <th>#</th>
                            <th>Merk</th>
                            <th>Ukuran</th>
                            <th>Jenis</th>
                            <th>Warna</th>
                        </tr>
                      </thead>
                      <tbody align="center">
                            </tr>
                            <?php
                            foreach ($barang_data as $barang)
                            {
                            ?>
                        <tr>
                            <td width="80px"><?php echo ++$start ?></td>
                            <td><?php echo $barang->merk ?></td>
                            <td><?php echo $barang->ukuran ?></td>
                            <td><?php echo $barang->jenis ?></td>
                            <td><?php echo $barang->warna ?></td>
                            <!-- <td><?php echo $barang->created_at ?></td>
                            <td><?php echo $barang->updated_at ?></td> -->
                        </tr>
                                <?php
                            }
                            ?>
                      </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-6">
                          <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
                          <?php echo anchor(site_url('barang/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                          <?php echo anchor(site_url('barang/word'), 'Word', 'class="btn btn-primary"'); ?>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php echo $pagination ?>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
         