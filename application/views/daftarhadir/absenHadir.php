        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Welcome, <?php echo $this->session->userdata('namalengkap');?> </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url('Daftarhadir') ?>">List</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Absensi</li>
                </ol>
              </nav>
            </div>
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Absen : <?php echo date("d-M-Y");?></h4>
                    <p class="card-description"> Absen <code>Karyawan</code>
                    </p>
                    <form action="<?php echo base_url('Daftarhadir/prosesAbsen');?>" method="POST">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th> Nama Karyawan </th>
                          <th> Jabatan </th>
                          <th> Status Absen </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($karyawan as $key){?>
                        <tr>
                          <td class="py-1">
                            <?php echo  $key->namalengkap;?>
                          </td>
                          <td> 
                            <?php if($key->akses == 1){echo "Admin";}elseif($key->akses == 2){echo "Operator";}elseif($key->akses == 3){echo "Kepala";}elseif($key->akses == 4){echo "Penjaga Gudang";}?>
                          </td>

                          <td>
                                <input type="radio" name="optionsRadios<?php echo $key->id_users;?>" id="optionsRadios1" value="Hadir" checked> Hadir 
                                <input type="radio" name="optionsRadios<?php echo $key->id_users;?>" id="optionsRadios2" value="Sakit" > Sakit 
                                <input type="radio" name="optionsRadios<?php echo $key->id_users;?>" id="optionsRadios3" value="Alpha" > Alpha 
                                <input type="radio" name="optionsRadios<?php echo $key->id_users;?>" id="optionsRadios4" value="Izin"> Izin
                          </td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                    <button class="btn btn-primary float-right"> Submit Absen </button>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->