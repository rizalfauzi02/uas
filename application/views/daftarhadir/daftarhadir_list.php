        <!-- CONTENT ==================== -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-account"></i>
                </span> Daftar Hadir
              </h3>
            </div>
<?= $this->session->flashdata('pesan'); ?>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Barang -->
                    <!-- <button type="button" class="btn btn-gradient-primary btn-fw float-right">Tambah User</button> -->
                    <a href="<?= base_url('daftarhadir/absenCreate') ?>" class="btn btn-gradient-primary btn-fw float-right">Absen</a><br>
                    <div class="col-md-8 text-right">
                        <form action="<?php echo site_url('daftarhadir/index'); ?>" class="form-inline" method="get">
                            <div class="input-group">
                                <input type="date" class="form-control" id="tanggal" name="q" value="<?php echo $q; ?>">
                                <span class="input-group-btn">
                                    <?php 
                                        if ($q <> '')
                                        {
                                            ?>
                                            <a href="<?php echo site_url('daftarhadir'); ?>" class="btn btn-default">Reset</a>
                                            <?php
                                        }
                                    ?>
                                <button class="btn btn-primary" id="search" type="submit">Search by Tanggal</button>
                                </span>
                            </div>
                        </form> 
                    </div> <br>                      
                        <table class="table table-striped">
                      <thead>
                        <tr align="center">
                            <th>#</th>
                            <th>Nama Karyawan</th>
                            <th>Status</th>
                            <th>Tanggal Absen</th>
                        </tr>
                      </thead>
                      <tbody align="center">
                            </tr>
                                <?php
                                foreach ($daftarhadir_data as $daftarhadir)
                                {
                                    ?>
                                    <tr>
                                <td width="80px"><?php echo ++$start ?></td>
                                <td><?php echo $this->db->get_where('users',array('id_users' => $daftarhadir->id_pengguna))->row('namalengkap'); ?></td>
                                <td><?php echo $daftarhadir->status ?></td>
                                <td><?php echo $daftarhadir->created_at ?></td>
                                <td style="text-align:center" width="200px">
                                    <?php 
                                    // echo anchor(site_url('daftarhadir/read/'.$daftarhadir->id),'Read'); 
                                    // echo ' | '; 
                                    // echo anchor(site_url('daftarhadir/update/'.$daftarhadir->id),'Update'); 
                                    // echo ' | '; 
                                    // echo anchor(site_url('daftarhadir/delete/'.$daftarhadir->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                                    ?>
                                <a href="<?= base_url('daftarhadir/delete/') . $daftarhadir->id; ?>" style="text-decoration:none" 
                                  class="btn btn-gradient-danger btn-sm hapus" onclick="return confirm('Data akan dihapus?')">  <!-- SWEETALERT BUTTON BELUM JALAN -->
                                    <!-- <i class="fas fa-trash"></i> -->Delete
                                </a>
                                </td>
                            </tr>
                                    <?php
                                }
                                ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
         