        <!-- CONTENT ==================== -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-account"></i>
                </span> Barang List
              </h3>
            </div>
<?= $this->session->flashdata('pesan'); ?>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Barang -->
                    <!-- <button type="button" class="btn btn-gradient-primary btn-fw float-right">Tambah User</button> -->
                    <div class="col-md-4">
                            <form action="<?php echo site_url('barang/list'); ?>" class="form-inline" method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                                        <?php 
                                            if ($q <> '')
                                        {
                                        ?>
                                            <a href="<?php echo site_url('barang/list'); ?>" class="btn btn-default">Reset</a>
                                        <?php
                                        }
                                        ?>
                                        <button class="btn btn-primary" type="submit">Search</button>
                                </div>
                            </form>
                    </div>                        
                        <a href="<?= base_url('barang/create') ?>" class="btn btn-gradient-primary btn-fw float-right">Tambah Barang</a><br>
                        <table class="table table-striped">
                      <thead>
                        <tr align="center">
                            <th>#</th>
                            <th>Merk</th>
                            <th>Ukuran</th>
                            <th>Jenis</th>
                            <th>Warna</th>
                            <!-- <th>Created At</th>
                            <th>Updated At</th> -->
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody align="center">
                            </tr>
                            <?php
                            foreach ($barang_data as $barang)
                            {
                            ?>
                        <tr>
                            <td width="80px"><?php echo ++$start ?></td>
                            <td><?php echo $barang->merk ?></td>
                            <td><?php echo $barang->ukuran ?></td>
                            <td><?php echo $barang->jenis ?></td>
                            <td><?php echo $barang->warna ?></td>
                            <!-- <td><?php echo $barang->created_at ?></td>
                            <td><?php echo $barang->updated_at ?></td> -->
                            <td style="text-align:center" width="200px">
                                <?php 
                                echo anchor(site_url('barang/read/'.$barang->id),'Read', 'class="btn btn-gradient-primary btn-sm"'); 
                                echo ' | '; 
                                echo anchor(site_url('barang/update/'.$barang->id),'Update', 'class="btn btn-gradient-success btn-sm"'); 
                                echo ' | '; 
                                // echo anchor(site_url('barang/delete/'.$barang->id),'Delete','onclick="javasciprt: return confirm(\'Data akan dihapus?\')"'); 
                                ?>
                                <a href="<?= base_url('barang/delete/') . $barang->id; ?>" style="text-decoration:none" 
                                  class="btn btn-gradient-danger btn-sm hapus" onclick="return confirm('Data akan dihapus?')">  <!-- SWEETALERT BUTTON BELUM JALAN -->
                                    <!-- <i class="fas fa-trash"></i> -->Delete
                                </a>
                            </td>
                        </tr>
                                <?php
                            }
                            ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
         