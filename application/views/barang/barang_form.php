        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Input Barang </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url('Barang/list') ?>">Barang</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Tambah Barang</li>
                </ol>
              </nav>
            </div>
            <div class="row">
              <div class="col-md-9 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Tambah Barang</h4><br>
                    
                <form action="<?php echo $action; ?>" method="post">
                    <div class="form-group">
                        <label for="varchar">Merk <?php echo form_error('merk') ?></label>
                        <input type="text" class="form-control" name="merk" id="merk" placeholder="Merk" value="<?php echo $merk; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="varchar">Ukuran <?php echo form_error('ukuran') ?></label>
                        <input type="text" class="form-control" name="ukuran" id="ukuran" placeholder="Ukuran" value="<?php echo $ukuran; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="varchar">Jenis <?php echo form_error('jenis') ?></label>
                        <input type="text" class="form-control" name="jenis" id="jenis" placeholder="Jenis" value="<?php echo $jenis; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="varchar">Warna <?php echo form_error('warna') ?></label>
                        <input type="text" class="form-control" name="warna" id="warna" placeholder="Warna" value="<?php echo $warna; ?>" />
                    </div>
                    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                    <a href="<?php echo site_url('barang/list') ?>" class="btn btn-default">Cancel</a>
                </form>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <!-- partial -->
        </div>