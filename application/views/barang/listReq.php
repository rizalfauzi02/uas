        <!-- CONTENT ==================== -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-format-list-bulleted"></i>
                </span> List Request
              </h3>
            </div>
<?= $this->session->flashdata('pesan'); ?>
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">                     
                        <!-- <a href="<?= base_url('dept/req') ?>" class="btn btn-gradient-primary btn-fw float-right">Request</a><br> -->
                        <table class="table table-striped">
                      <thead>
                        <tr align="center">
                            <th>#</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                            <th>Harga Satuan</th>
                            <th>Total Harga</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody align="center">
                            </tr>
                            <?php $no=1;
                            foreach ($list as $row)
                            {
                            ?>
                        <tr>
                            <td width="80px"><?php echo $no++ ?></td>
                            <td><?php echo $row->namabarang ?></td>
                            <td><?php echo $row->qty ?></td>
                            <td><?php echo $row->satuan ?></td>
                            <td><?php echo $row->harga ?></td>
                            <td><?php echo $row->totharga ?></td>
                            <td><label class="badge badge-gradient-danger"><?= $row->status; ?></td>
                            

                            <td style="text-align:center">
                                <a href="<?= base_url('barang/acc/') . $row->id_request; ?>" style="text-decoration:none" 
                                  class="btn btn-gradient-success btn-sm" onclick="return confirm('Terima Barang?')">  <!-- SWEETALERT BUTTON BELUM JALAN -->
                                    <i class="fas fa-trash"></i>Terima
                                </a>
                                |
                                <a href="<?= base_url('barang/rej/') . $row->id_request; ?>" style="text-decoration:none" 
                                  class="btn btn-gradient-danger btn-sm" onclick="return confirm('Tolak Barang?')">  <!-- SWEETALERT BUTTON BELUM JALAN -->
                                    <i class="fas fa-trash"></i>Tolak
                                </a>
                            </td>
                        </tr>
                                <?php
                            }
                            ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
         