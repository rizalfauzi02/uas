      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="nav-profile-image">
                  <img src="<?= base_url('assets-app/') ?>assets/images/user.png" alt="profile">
                  <span class="login-status online"></span>
                  <!--change to offline or busy as needed-->
                </div>
                <div class="nav-profile-text d-flex flex-column">
                  <span class="font-weight-bold mb-2"><?= $users['namalengkap']; ?></span>
                  <?php if($users['akses'] == 1) : ?>
                    <span class="text-secondary text-small">ADMIN</span>
                  <?php elseif($users['akses'] == 2) : ?>
                    <span class="text-secondary text-small">OPERATOR</span>
                  <?php elseif($users['akses'] == 3) : ?>
                    <span class="text-secondary text-small">KEPALA GUDANG</span>
                  <?php elseif($users['akses'] == 4) : ?>
                    <span class="text-secondary text-small">PENJAGA GUDANG</span>
                  <?php elseif($users['akses'] == 5) : ?>
                    <span class="text-secondary text-small">DEPT PENJUALAN</span>
                  <?php endif; ?>
                </div>
                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
              </a>
            </li>

            <li class="nav-item <?= $menu_view['pengguna']; ?>">
              <a class="nav-link" href="<?= base_url('Kepala/reportUsers') ?>">
                <span class="menu-title">Report Pengguna</span>
                <i class="mdi mdi-bookmark menu-icon"></i>
              </a>
            </li>

            <li class="nav-item <?= $menu_view['barang']; ?>">
              <a class="nav-link" href="<?= base_url('Kepala/reportBarang') ?>">
                <span class="menu-title">Report Barang</span>
                <i class="mdi mdi-book-multiple-variant menu-icon"></i>
              </a>
            </li>

            <li class="nav-item <?= $menu_view['kehadiran']; ?>">
              <a class="nav-link" href="<?= base_url('Kepala/reportKehadiran') ?>">
                <span class="menu-title">Report Kehadiran</span>
                <i class="mdi mdi-book-multiple menu-icon"></i>
              </a>
            </li>
          </ul>
        </nav>
        <!-- partial -->