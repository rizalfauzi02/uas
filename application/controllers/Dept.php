<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dept extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // $this->load->model('Daftarhadir_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        // ACTIVE  MENU
        $menu = [
            'req' => ''
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;
        $data = array(
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/dept', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('dept/index', $data);
        $this->load->view('tamplates/footer');
    }

    public function request()
    {
        // ACTIVE  MENU
        $menu = [
            'req' => 'active'
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;
        $data = array(
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );

        $data['list'] = $this->db->get('request')->result();
        // var_dump($data['list']);die;

        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/dept', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('dept/listReq', $data);
    }

    public function req()
    {
        // ACTIVE  MENU
        $menu = [
            'req' => 'active'
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;
        $data = array(
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );

        // Mengambil data barang
        $data['barang'] = $this->db->get('barang')->result();
        // var_dump($data['barang']); die;

        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/dept', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('dept/req', $data);
    }

    public function prosesSubmit()
    {
        // var_dump($_POST); die;
        $this->form_validation->set_rules('namabarang', 'Nama Barang', 'required');
        $this->form_validation->set_rules('qty', 'qty', 'required');

        if ($this->form_validation->run() == false) {
            redirect('dept/req');
        } else {
            $data['namabarang'] = $this->input->post('namabarang');
            $data['qty'] = $this->input->post('qty');
            $data['satuan'] = $this->input->post('satuan');
            $data['harga'] = $this->input->post('harga');
            $data['totharga'] = $this->input->post('totharga');
            $data['status'] = $this->input->post('status');

            $this->db->insert('request', $data);
            $this->session->set_flashdata('pesan', "
                <script>
                    swal('Berhasil!', 'Form Request berhasil dikirim!', 'success');
                </script>
            ");
            redirect('dept/request');
        }
    }
}