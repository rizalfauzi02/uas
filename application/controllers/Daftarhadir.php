<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Daftarhadir extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Daftarhadir_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'daftarhadir/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'daftarhadir/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'daftarhadir/index.html';
            $config['first_url'] = base_url() . 'daftarhadir/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Daftarhadir_model->total_rows($q);
        $daftarhadir = $this->Daftarhadir_model->get_limit_data($config['per_page'], $start, $q);

        // ACTIVE  MENU
        $menu = [
            'Daftar' => 'Active'
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'daftarhadir_data' => $daftarhadir,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/penjaga', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('daftarhadir/daftarhadir_list', $data);
        $this->load->view('tamplates/footer');
    }

    public function read($id) 
    {
        $row = $this->Daftarhadir_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'id_pengguna' => $row->id_pengguna,
		'status' => $row->status,
		'created_at' => $row->created_at,
		'updated_at' => $row->updated_at,
	    );
            $this->load->view('daftarhadir/daftarhadir_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('daftarhadir'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('daftarhadir/create_action'),
	    'id' => set_value('id'),
	    'id_pengguna' => set_value('id_pengguna'),
	    'status' => set_value('status'),
	    'created_at' => set_value('created_at'),
	    'updated_at' => set_value('updated_at'),
	);
        $this->load->view('daftarhadir/daftarhadir_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_pengguna' => $this->input->post('id_pengguna',TRUE),
		'status' => $this->input->post('status',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->Daftarhadir_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('daftarhadir'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Daftarhadir_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('daftarhadir/update_action'),
		'id' => set_value('id', $row->id),
		'id_pengguna' => set_value('id_pengguna', $row->id_pengguna),
		'status' => set_value('status', $row->status),
		'created_at' => set_value('created_at', $row->created_at),
		'updated_at' => set_value('updated_at', $row->updated_at),
	    );
            $this->load->view('daftarhadir/daftarhadir_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('daftarhadir'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'id_pengguna' => $this->input->post('id_pengguna',TRUE),
		'status' => $this->input->post('status',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->Daftarhadir_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('daftarhadir'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Daftarhadir_model->get_by_id($id);

        if ($row) {
            $this->Daftarhadir_model->delete($id);
            $this->session->set_flashdata('pesan', "
                <script>
                    swal('Berhasil!', 'Data Berhasil dihapus!', 'success');
                </script>
            ");
            redirect(site_url('daftarhadir'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('daftarhadir'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_pengguna', 'id pengguna', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('created_at', 'created at', 'trim|required');
	$this->form_validation->set_rules('updated_at', 'updated at', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "daftarhadir.xls";
        $judul = "daftarhadir";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Pengguna");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated At");

    // $user = $this->db->get_where('users',array('id_users' => $daftarhadir->id_pengguna))->row('namalengkap');

	foreach ($this->Daftarhadir_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_pengguna);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_at);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=daftarhadir.doc");

        $data = array(
            'daftarhadir_data' => $this->Daftarhadir_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('daftarhadir/daftarhadir_doc',$data);
    }

    public function dashboard()
    {
        // ACTIVE  MENU
        $menu = [
            'Daftar' => ''
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;
        $data = array(
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/penjaga', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('daftarhadir/dashboard', $data);
        $this->load->view('tamplates/footer');
    }

    public function absenCreate(){
        // ACTIVE  MENU
        $menu = [
            'Daftar' => 'Active'
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;
        $data = array(
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );

        $data['karyawan'] = $this->db->get('users')->result();
        // var_dump($data['karyawan']); die;
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/penjaga', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('daftarhadir/absenHadir',$data);
        $this->load->view('tamplates/footer');
    }

    public function prosesAbsen(){
        foreach ($_POST as $key => $value) {
            $id_pengguna = substr($key,13);
            $status = $value;
            $data = array(
                    'id_pengguna' => $id_pengguna,
                    'status' => $status
            );

            $this->db->insert('daftarhadir', $data);
        }
        redirect('Daftarhadir');
    }

}

/* End of file Daftarhadir.php */
/* Location: ./application/controllers/Daftarhadir.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2021-08-07 08:20:49 */
/* http://harviacode.com */