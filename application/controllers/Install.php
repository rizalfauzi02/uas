<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Install extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }
    public function index()
    {
        $data['title'] = 'Register Admin';
        $this->load->view('reg', $data);
       
    }

    public function prosesReg()
    {
        // var_dump($_POST); die;
        // Rules
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        
        if ($this->form_validation->run() == false) {
            $data['title'] = 'Register Admin';
            $this->load->view('reg', $data);
        } else {
            $this->load->model('M_register');

            $data['namalengkap'] = $this->input->post('namalengkap');
            $data['email'] = $this->input->post('email');
            $data['username'] = $this->input->post('username');
            $password = $this->input->post('password1');
            $data['akses'] = 1;
            $data['isActive'] = 1;
            $password2 = $this->input->post('password2');
    
            // validasi
            if($password == $password2){
                // echo "Password sama!";
                $options = [
                    'cost' => 5,
                ];
                // echo password_hash($password, PASSWORD_DEFAULT, $options);
    
                $data['password'] = password_hash($password, PASSWORD_DEFAULT, $options);
                $this->M_register->insertUser($data);
                $this->session->set_flashdata('pesan', "
                <script>
                    swal('Berhasil!', 'Anda Berhasil Mendaftar. Silahkan Login', 'success');
                </script>
                ");
                redirect('auth');
                
            }
            else{
                $this->session->set_flashdata('pesan', "
                <script>
                    swal('Gagal!', 'Password tidak sama!', 'error');
                </script>
                ");
                redirect('install');
            }
        }

    }
}
