<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kepala extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Users2_model');
        $this->load->model('Barang_model');
        $this->load->model('Daftarhadir_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        // ACTIVE  MENU
        $menu = [
            'pengguna' => '',
            'barang' => '',
            'kehadiran' => '',
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;
        $data = array(
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/kepala', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('kepala/dashboard', $data);
        $this->load->view('tamplates/footer');
    }

// REPORT USERS =================================================
    public function reportUsers()
    {

        // ACTIVE  MENU
        $menu = [
            'pengguna' => 'Active',
            'barang' => '',
            'kehadiran' => '',
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;

        // redirect('Users1');
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'users1/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'users1/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'users1/index.html';
            $config['first_url'] = base_url() . 'users1/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Users2_model->total_rows($q);
        $users1 = $this->Users2_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'users1_data' => $users1,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/kepala', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('kepala/users1/users_list', $data);
        $this->load->view('tamplates/footer');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "users.xls";
        $judul = "users";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Namalengkap");
	xlsWriteLabel($tablehead, $kolomhead++, "Username");
	xlsWriteLabel($tablehead, $kolomhead++, "Email");
	xlsWriteLabel($tablehead, $kolomhead++, "Akses");
	xlsWriteLabel($tablehead, $kolomhead++, "IsActive");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated At");

	foreach ($this->Users2_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->namalengkap);
	    xlsWriteLabel($tablebody, $kolombody++, $data->username);
	    xlsWriteLabel($tablebody, $kolombody++, $data->email);
	    xlsWriteNumber($tablebody, $kolombody++, $data->akses);
	    xlsWriteNumber($tablebody, $kolombody++, $data->isActive);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_at);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=users.doc");

        $data = array(
            'users1_data' => $this->Users2_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('kepala/users1/users_doc',$data);
    }
// END REPORT USERS ============================================

// REPORT BARANG =================================================
public function reportBarang()
{
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'barang/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'barang/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'barang/index.html';
            $config['first_url'] = base_url() . 'barang/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Barang_model->total_rows($q);
        $barang = $this->Barang_model->get_limit_data($config['per_page'], $start, $q);

        // ACTIVE  MENU
        $menu = [
            'pengguna' => '',
            'barang' => 'active',
            'kehadiran' => '',
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'barang_data' => $barang,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/kepala', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('kepala/barang/barang_list', $data);
        $this->load->view('tamplates/footer');
}
// END REPORT BARANG ============================================

// REPORT KEHADIRAN =================================================
public function reportKehadiran()
{
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'daftarhadir/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'daftarhadir/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'daftarhadir/index.html';
            $config['first_url'] = base_url() . 'daftarhadir/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Daftarhadir_model->total_rows($q);
        $daftarhadir = $this->Daftarhadir_model->get_limit_data($config['per_page'], $start, $q);

        // ACTIVE  MENU
        $menu = [
            'pengguna' => '',
            'barang' => '',
            'kehadiran' => 'active',
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'daftarhadir_data' => $daftarhadir,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/kepala', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('kepala/daftarhadir/daftarhadir_list', $data);
        $this->load->view('tamplates/footer');
}
// END REPORT KEHADIRAN ============================================
}