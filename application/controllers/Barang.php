<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Barang extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Barang_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'barang/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'barang/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'barang/index.html';
            $config['first_url'] = base_url() . 'barang/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Barang_model->total_rows($q);
        $barang = $this->Barang_model->get_limit_data($config['per_page'], $start, $q);

        // ACTIVE  MENU
        $menu = [
            'Barang' => '',
            'Req'    => ''
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'barang_data' => $barang,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );
        // $this->load->view('barang/barang_list', $data);
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/operator', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('barang/dashboard', $data);
        $this->load->view('tamplates/footer');
    }

    public function list()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'barang/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'barang/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'barang/index.html';
            $config['first_url'] = base_url() . 'barang/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Barang_model->total_rows($q);
        $barang = $this->Barang_model->get_limit_data($config['per_page'], $start, $q);

        // ACTIVE  MENU
        $menu = [
            'Barang' => 'active',
            'Req'    => ''
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'barang_data' => $barang,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/operator', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('barang/barang_list', $data);
        $this->load->view('tamplates/footer');
    }

    public function read($id) 
    {
        $row = $this->Barang_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'merk' => $row->merk,
		'ukuran' => $row->ukuran,
		'jenis' => $row->jenis,
		'warna' => $row->warna,
		'created_at' => $row->created_at,
		'updated_at' => $row->updated_at,
	    );
            $this->load->view('barang/barang_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function create() 
    {
        // ACTIVE  MENU
        $menu = [
            'Barang' => 'active',
            'Req'    => ''
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;

        $data = array(
            'button' => 'Create',
            'action' => site_url('barang/create_action'),
	    'id' => set_value('id'),
	    'merk' => set_value('merk'),
	    'ukuran' => set_value('ukuran'),
	    'jenis' => set_value('jenis'),
	    'warna' => set_value('warna'),
	    // 'created_at' => set_value('created_at'),
	    // 'updated_at' => set_value('updated_at'),
        'menu_view' => $menu, // MENU DI OPER KE SINI
        'users' => $user
	);
        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/operator', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('barang/barang_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'merk' => $this->input->post('merk',TRUE),
		'ukuran' => $this->input->post('ukuran',TRUE),
		'jenis' => $this->input->post('jenis',TRUE),
		'warna' => $this->input->post('warna',TRUE),
		'created_at' => $this->input->post('created_at',TRUE),
		'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->Barang_model->insert($data);
            $this->session->set_flashdata('pesan', "
                <script>
                    swal('Berhasil!', 'Barang berhasil ditambahkan!', 'success');
                </script>
            ");
            redirect(site_url('barang/list'));
        }
    }
    
    public function update($id) 
    {
        // ACTIVE  MENU
        $menu = [
            'Barang' => 'active',
            'Req'    => ''
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;

        $row = $this->Barang_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('barang/update_action'),
		'id' => set_value('id', $row->id),
		'merk' => set_value('merk', $row->merk),
		'ukuran' => set_value('ukuran', $row->ukuran),
		'jenis' => set_value('jenis', $row->jenis),
		'warna' => set_value('warna', $row->warna),
        'menu_view' => $menu, // MENU DI OPER KE SINI
        'users' => $user
	    );
            $this->load->view('tamplates/header', $data);
            $this->load->view('tamplates/sidebar/operator', $data); // SIDEBAR SETIAP USER BERBEDA
            $this->load->view('barang/barang_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'merk' => $this->input->post('merk',TRUE),
		'ukuran' => $this->input->post('ukuran',TRUE),
		'jenis' => $this->input->post('jenis',TRUE),
		'warna' => $this->input->post('warna',TRUE)
		// 'created_at' => $this->input->post('created_at',TRUE),
		// 'updated_at' => $this->input->post('updated_at',TRUE),
	    );

            $this->Barang_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('pesan', "
                <script>
                    swal('Berhasil!', 'Barang berhasil diupdate!', 'success');
                </script>
            ");
            redirect(site_url('barang/list'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Barang_model->get_by_id($id);

        if ($row) {
            $this->Barang_model->delete($id);
            $this->session->set_flashdata('pesan', "
                <script>
                    swal('Berhasil!', 'Data Barang Berhasil dihapus!', 'success');
                </script>
            ");
            redirect(site_url('barang/list'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('merk', 'merk', 'trim|required');
	$this->form_validation->set_rules('ukuran', 'ukuran', 'trim|required');
	$this->form_validation->set_rules('jenis', 'jenis', 'trim|required');
	$this->form_validation->set_rules('warna', 'warna', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "barang.xls";
        $judul = "barang";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Merk");
	xlsWriteLabel($tablehead, $kolomhead++, "Ukuran");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis");
	xlsWriteLabel($tablehead, $kolomhead++, "Warna");
	xlsWriteLabel($tablehead, $kolomhead++, "Created At");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated At");

	foreach ($this->Barang_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->merk);
	    xlsWriteLabel($tablebody, $kolombody++, $data->ukuran);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jenis);
	    xlsWriteLabel($tablebody, $kolombody++, $data->warna);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_at);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_at);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=barang.doc");

        $data = array(
            'barang_data' => $this->Barang_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('kepala/barang/barang_doc',$data);
    }

    // REQUEST BARANG ==============================================
    public function listRequest()
    {
        // ACTIVE  MENU
        $menu = [
            'Barang' => '',
            'Req'    => 'active'
        ];
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        // var_dump($user); die;
        $data = array(
            'menu_view' => $menu, // MENU DI OPER KE SINI
            'users' => $user
        );

        $data['list'] = $this->db->get('request')->result();
        // var_dump($data['list']);die;

        $this->load->view('tamplates/header', $data);
        $this->load->view('tamplates/sidebar/operator', $data); // SIDEBAR SETIAP USER BERBEDA
        $this->load->view('barang/listReq', $data);
    }
    public function acc($id)
    {
        $data = array(
        'status' => 'Di Terima'
        );
        // var_dump($data); die;

        $this->db->where('id_request', $id);
        $this->db->update('request', $data);
        $this->session->set_flashdata('pesan', "
            <script>
                swal('Berhasil!', 'Request Di Terima!', 'success');
            </script>
        ");
        redirect('barang/listRequest');
    }

    public function rej($id)
    {
        $data = array(
        'status' => 'Di Tolak'
        );
        // var_dump($data); die;

        $this->db->where('id_request', $id);
        $this->db->update('request', $data);
        $this->session->set_flashdata('pesan', "
            <script>
                swal('Berhasil!', 'Request Di Tolak!', 'warning');
            </script>
        ");
        redirect('barang/listRequest');
    }
    // END REQUEST BARANG ==============================================

}

/* End of file Barang.php */
/* Location: ./application/controllers/Barang.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2021-08-07 05:17:04 */
/* http://harviacode.com */