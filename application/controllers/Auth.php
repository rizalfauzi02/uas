<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->model('M_login');
        $this->load->model('M_register');
        if (count($this->db->get_where('users', ['akses' => 1])->result()) == 0) {
            redirect('install');
        }

        // if($this->session->has_userdata('isLoggin') != true){
        //     redirect('auth');
        // }
        
    }
    
	public function index()
	{
		$data['title'] = 'Halaman Login';
		$this->load->view('tamplates/auth_header', $data);
        $this->load->view('auth/login', $data);
        $this->load->view('tamplates/auth_footer');
        // echo "TEST";
	}

	// PROSES
	public function prosessLogin()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$getUser = $this->M_login->getUser($username);
		// var_dump($getUser); die;

		// PROSES PENGECEKAN
        if(count($getUser) == 1){
            if(password_verify($password, $getUser[0]->password)){

                $dataSession = array(
                    "namalengkap" => $getUser[0]->namalengkap,
                    "username"    => $getUser[0]->username,
                    "isActive"    => $getUser[0]->isActive,
                    "akses"       => $getUser[0]->akses,
                    "isLoggin" => true
                    // true = 1
                );
                $this->session->set_userdata($dataSession);

                if($getUser[0]->isActive == 1){
                    if($getUser[0]->akses == 1){
                        redirect('Admin');
                        // echo "berhasil masuk!";
                    } elseif($getUser[0]->akses == 2){
                        redirect('barang');
                        // echo "berhasil masuk!";
                    } elseif($getUser[0]->akses == 3){
                        redirect('kepala');
                        // echo "berhasil masuk!";
                    } elseif($getUser[0]->akses == 4){
                        redirect('daftarhadir/dashboard');
                        // echo "berhasil masuk!";
                    } elseif($getUser[0]->akses == 5){
                        redirect('dept');
                        // echo "berhasil masuk!";
                    }
                    
                }
            }else{
                $data['title'] = 'Warehouse';
                // $data['peringatan'] = "Password Anda salah!.";
                    
                // $this->load->view('templates/auth_header', $data);
                // $this->load->view('auth/v_gagallogin', $data);
                // $this->load->view('templates/auth_footer');
                // echo "<script>
                //     alert('Password Salah!');    
                //     window.location.href='". base_url() ."auth';
                // </script>";
                $this->session->set_flashdata('pesan', "
                <script>
                    swal('Password Salah!', 'Silahkan diperiksa kembali!', 'error');
                </script>
                ");
                redirect('auth');
            }
        }elseif(count($getUser) == 0){
            $data['title'] = 'Warehouse';
            // $data['peringatan'] = "Anda belum terdaftar!";
            
            // $this->load->view('templates/auth_header', $data);
            // $this->load->view('auth/v_gagallogin', $data);
            // $this->load->view('templates/auth_footer');
                // echo "<script>
                //     alert('Akun tidak ditemukan!');    
                //     window.location.href='". base_url() ."auth';
                // </script>";
                $this->session->set_flashdata('pesan', "
                <script>
                    swal('Akun tidak ditemukan!', 'Silahkan hubungi admin!', 'warning');
                </script>
                ");
                redirect('auth');
        }
        else{
            // $this->session->set_flashdata('pesan', "
            // <script>
            //    Swal.fire({
            //         icon: 'error',
            //         title: 'Oops..!',
            //         text: 'Username Tidak ditemukan!',
            //         })
            // </script>
            // ");
            // redirect('auth');
                echo "<script>
                    alert('Username tidak ditemukan!');    
                    window.location.href='". base_url() ."auth';
                </script>";
        }
	}

    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('password');
        $this->session->unset_userdata('akses');
        redirect('auth');
    }
}
